package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddRemoveProducts {

    private WebDriver driver;

    public AddRemoveProducts(WebDriver driver) {
        this.driver = driver;
    }
    private By addToCartBackpackProduct = By.id("add-to-cart-sauce-labs-backpack");
    private By addToCartBikeLightProduct = By.id("add-to-cart-sauce-labs-bike-light");
    private By addToCartBoltShirtProduct = By.id("add-to-cart-sauce-labs-bolt-t-shirt");
    private By addToCartFleeceJacketProduct = By.id("add-to-cart-sauce-labs-fleece-jacket");
    private By addToCartOnesieProduct = By.id("add-to-cart-sauce-labs-onesie");
    private By addToCartRedShirtProduct = By.id("add-to-cart-test.allthethings()-t-shirt-(red)");

    private By removeFromCartBackpackProduct = By.id("remove-sauce-labs-backpack");

    private By removeFromCartBikeLight = By.id("remove-sauce-labs-bike-light");

    private By removeFromCartBoltShortProduct = By.id("remove-sauce-labs-bolt-t-shirt");

    private By removeFromCartFleeceJacketProduct = By.id("remove-sauce-labs-fleece-jacket");

    private By removeFromCartOnesieProduct = By.id("remove-sauce-labs-onesie");

    private By removefromCartRedShirtProduct = By.id("remove-test.allthethings()-t-shirt-(red)");



    public void addToCartBackpackProductButton() throws InterruptedException {
        driver.findElement(addToCartBackpackProduct).click();
        Thread.sleep(2000);
        driver.findElement(removeFromCartBackpackProduct).click();
        Thread.sleep(2000);
    }

    public void addToCartBikeLightButton() {
        driver.findElement(addToCartBikeLightProduct).click();
    }

    public void addToCartBoltShirtButton() {
        driver.findElement(addToCartBoltShirtProduct).click();
    }

    public void addToCartFleeceJacketButton() {
        driver.findElement(addToCartFleeceJacketProduct).click();
    }

    public void addToCartOnesieButton() {
        driver.findElement(addToCartOnesieProduct).click();
    }

    public void addToCartRedShirtButton() {
        driver.findElement(addToCartRedShirtProduct).click();

    }

}
