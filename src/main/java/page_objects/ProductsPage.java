package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class ProductsPage{

    private WebDriver driver;

    private By shoppingCart = By.xpath("//*[@id=\"shopping_cart_container\"]/a");


    private By openHamburgerMenu = By.id("react-burger-menu-btn");

    private By closeHamburgerMenu = By.id("react-burger-cross-btn");

    private By filterSelector = By.cssSelector("select[data-test=\"product_sort_container\"]");

public ProductsPage(WebDriver driver)
{
    this.driver=driver;

}
    public void filterZAProducts(){
        Select filterProducts = new Select(driver.findElement(filterSelector));
        filterProducts.selectByVisibleText("Name (Z to A)");
    }

    public void filterLoHiProducts(){
        Select filterProducts = new Select(driver.findElement(filterSelector));
        filterProducts.selectByVisibleText("Price (low to high)");
    }

    public void shoppingCart() throws InterruptedException{

    driver.findElement(shoppingCart).click();
    Thread.sleep(2000);
    }

    public void hamburgerMenu() throws InterruptedException {
        driver.findElement(openHamburgerMenu).click();
        Thread.sleep(2000);
    }

}
