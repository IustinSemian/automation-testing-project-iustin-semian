package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hooks {
    protected static WebDriver driver;

    @Before
    public void setupBefore() {
        System.out.println("Initialized driver-ul in before");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @After
    public void tearDownAfter() {
        System.out.println("Inchidem driver-ul");
        driver.quit();
    }
}