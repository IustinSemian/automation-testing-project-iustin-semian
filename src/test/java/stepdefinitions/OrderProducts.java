package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import page_objects.AddRemoveProducts;
import page_objects.Application;
import page_objects.LoginPage;

import static stepdefinitions.Hooks.driver;

public class OrderProducts {

    LoginPage order;
    @Given("Access the login page")
    public void accessTheLoginPage() {
        driver.get("https://www.saucedemo.com/");

        System.out.println("Open login page");
        order = new Application(driver).navigateToLoginPage();
    }

    @When("Insert the username")
    public void insertTheUsername() {
        order.fillUsername("standard_user");
        System.out.println("Fill username");

    }


    @And("Insert the password")
    public void insertThePassword() {
        order.fillPassword("secret_sauce");
        System.out.println("Fill password");
    }

    @And("Press the Login button")
    public void pressTheLoginButton() {
        order.submit();
    }


    @When("Add to cart Backpack")
    public void addToCartBackpack() throws InterruptedException {
        AddRemoveProducts orderSomething = new Application(driver).addProductsToCart();
        orderSomething.addToCartBackpackProductButton();

    }

    @And("Press the shopping cart button")
    public void pressTheShoppingCartButton() {
        driver.findElement(By.xpath("//*[@id=\"shopping_cart_container\"]/a")).click();


    }

    @And("Press the checkout button")
    public void pressTheCheckoutButton() {
        driver.findElement(By.id("checkout")).click();
    }

    @And("Fill the First name")
    public void fillTheFirstName() {
        driver.findElement(By.id("first-name")).sendKeys("Wantsome");

    }

    @And("Fill Last name")
    public void fillLastName() {
        driver.findElement(By.id("last-name")).sendKeys("Tester");

    }

    @And("Fill Postal code")
    public void fillPostalCode() {
        driver.findElement(By.id("postal-code")).sendKeys("1234");
    }

    @When("Press Continue button")
    public void pressContinueButton() {
        driver.findElement(By.id("continue")).click();
    }

    @And("Press the finish button")
    public void pressTheFinishButton() {
        driver.findElement(By.id("finish")).click();
    }

    @And("Press the back to products button")
    public void pressTheBackToProductsButton() {
        driver.findElement(By.xpath("//*[@id=\"back-to-products\"]")).click();

    }

    @And("Open the hamburger menu")
    public void openTheHamburgerMenu() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.id("react-burger-menu-btn")).click();
        Thread.sleep(2000);
    }

    @And("Press the logout button")
    public void pressTheLogoutButton() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.id("logout_sidebar_link")).click();

    }
}
