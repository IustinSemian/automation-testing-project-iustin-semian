package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_objects.Application;
import page_objects.LoginPage;
import static stepdefinitions.Hooks.driver;

public class LoginSteps {

    LoginPage login;

    @Given("Be on the login page")
    public void beOnTheLoginPage() {
        driver.get("https://www.saucedemo.com/");

        System.out.println("Open login page");
        login = new Application(driver).navigateToLoginPage();

    }

    @When("Put the username")
    public void putTheUserName() {
        login.fillUsername("standard_user");
        System.out.println("Fill username");

    }

    @And("Put the password")
    public void putThePassword() {
        login.fillPassword("secret_sauce");
        System.out.println("Fill password");
    }

    @When ("CLick Login button")
    public void clickLoginButton() {
        login.submit();
    }

    @Then("Check the logo displayed")
    public void checkTheProductsDisplayed() {
        Assert.assertEquals("Swag Labs",driver.getTitle());
    }
    

    @When("Put the problem username")
    public void putTheProblemUsername() {
        login.fillUsername("problem_user");
        System.out.println("Fill username");

    }

    @When("Put the locked out username")
    public void putTheLockedOutUsername() {
        login.fillUsername("locked_out_user");
        System.out.println("Fill username");

        }

    @When("Put the glitch username")
    public void putTheGlitchUsername() {
        login.fillUsername("performance_glitch_user");
        System.out.println("Fill username");
    }
}

