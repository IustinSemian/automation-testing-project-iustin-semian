package tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import utils.BaseClass;

import java.util.*;

public class AboutPage extends BaseClass {

    // Open in a new window the About category

    @Test
    public void windowsTest() throws InterruptedException {

        driver.get("https://www.saucedemo.com/inventory.html");
        Actions action = new Actions(driver);
        driver.findElement(By.id("react-burger-menu-btn")).click();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
        driver.findElement(By.id("about_sidebar_link"));
        action.moveToElement(driver.findElement(By.id("about_sidebar_link"))).keyDown(Keys.COMMAND).click().keyUp(Keys.COMMAND);
        action.build().perform();
        Thread.sleep(2000);
        Set<String> handle = driver.getWindowHandles();
        List<String> windows = new ArrayList<>();
        for (
                Iterator<String> iterator = handle.iterator(); iterator.hasNext(); ) {
            windows.add(iterator.next());
        }
        driver.switchTo().window(windows.get(1));
        //Thread.sleep(2000);
        Assert.assertEquals("https://saucelabs.com/",driver.getCurrentUrl());

    }
}
