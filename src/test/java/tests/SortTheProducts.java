package tests;

import org.junit.Assert;
import org.junit.Test;

import page_objects.AddRemoveProducts;
import page_objects.Application;
import page_objects.ProductsPage;
import utils.BaseClass;

public class SortTheProducts extends BaseClass {

    // Filter the products Z - A

    @Test
    public void sortZAProducts(){
        ProductsPage productsPage = new Application(driver).navigateToProductsPage();
        productsPage.filterZAProducts();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
    }

    // Filter the products Price (low to high)

    @Test
    public void sortLoHiPriceTheProducts(){
        ProductsPage productsPage = new Application (driver).navigateToProductsPage();
        productsPage.filterLoHiProducts();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
    }

    // Open-closing hamburger menu

    @Test
    public void openHamburgerMenu() throws InterruptedException {
        ProductsPage productsPage = new Application(driver).navigateToProductsPage();
        productsPage.hamburgerMenu();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
    }

    // Add and remove a product to shopping cart

    @Test
    public void addProductToCart() throws InterruptedException {
        AddRemoveProducts addRemoveProductsToCart = new Application(driver).addProductsToCart();
        addRemoveProductsToCart.addToCartBackpackProductButton();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
    }

    // Open the Shopping cart page

    @Test
    public void openShoppingCartPage() throws InterruptedException {
        ProductsPage checkCart = new Application(driver).navigateToProductsPage();
        checkCart.shoppingCart();
        Assert.assertEquals("https://www.saucedemo.com/cart.html",driver.getCurrentUrl());
    }


}

