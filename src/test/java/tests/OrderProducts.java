package tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import page_objects.AddRemoveProducts;
import page_objects.Application;
import utils.BaseClass;
import page_objects.ProductsPage;

public class OrderProducts extends BaseClass {

    // Order a Red T-Short from Swag Labs

    @Test
    public void orderProducts() throws InterruptedException{
        AddRemoveProducts orderSomething = new Application(driver).addProductsToCart();
        orderSomething.addToCartRedShirtButton();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
        driver.findElement(By.xpath("//*[@id=\"shopping_cart_container\"]/a")).click();
        Assert.assertEquals("https://www.saucedemo.com/cart.html",driver.getCurrentUrl());
        WebElement quantity = driver.findElement(By.xpath("//*[@id=\"cart_contents_container\"]/div/div[1]/div[3]/div[1]"));
        Assert.assertTrue(quantity.isDisplayed());
        Thread.sleep(2000);
        driver.findElement(By.id("checkout")).click();
        Assert.assertEquals("https://www.saucedemo.com/checkout-step-one.html",driver.getCurrentUrl());
        driver.findElement(By.id("first-name")).sendKeys("Wantsome");
        driver.findElement(By.id("last-name")).sendKeys("Tester");
        driver.findElement(By.id("postal-code")).sendKeys("1234");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("https://www.saucedemo.com/checkout-step-two.html",driver.getCurrentUrl());
        driver.findElement(By.id("finish")).click();
        Assert.assertEquals("https://www.saucedemo.com/checkout-complete.html",driver.getCurrentUrl());
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"back-to-products\"]")).click();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html",driver.getCurrentUrl());
        Thread.sleep(2000);
        driver.findElement(By.id("react-burger-menu-btn")).click();
        driver.findElement(By.id("logout_sidebar_link")).click();
        Assert.assertEquals("https://www.saucedemo.com/",driver.getCurrentUrl());
        Thread.sleep(2000);

    }

}


