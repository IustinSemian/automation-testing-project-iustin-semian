package tests;

import org.junit.Assert;
import org.junit.Test;


import org.junit.jupiter.api.BeforeEach;
import page_objects.Application;
import page_objects.LoginPage;
import utils.BaseClass;

// Positive test login with a standard user

public class Login extends BaseClass {

    @Test
    public void loginStandardUser() {

        LoginPage login = new Application(driver).navigateToLoginPage();
        login.fillUsername("standard_user");
        login.fillPassword("secret_sauce");
        login.submit();

        Assert.assertEquals("Swag Labs",driver.getTitle());
    }

// tests.Login with Locked Out user

    @Test
    public void loginLockedOutUser(){

    LoginPage login = new Application(driver).navigateToLoginPage();
        login.fillUsername("locked_out_user");
        login.fillPassword("secret_sauce");
        login.submit();

        Assert.assertEquals("Swag Labs",driver.getTitle());
}

// tests.Login with Problem user

    @Test
    public void loginProblemUser(){

        LoginPage login = new Application(driver).navigateToLoginPage();
        login.fillUsername("problem_user");
        login.fillPassword("secret_sauce");
        login.submit();

        Assert.assertEquals("Swag Labs",driver.getTitle());
    }

// tests.Login with Performance Glitch user

    @Test
    public void loginPerformanceGlitchUser(){

        LoginPage login = new Application(driver).navigateToLoginPage();
        login.fillUsername("performance_glitch_user");
        login.fillPassword("secret_sauce");
        login.submit();

        Assert.assertEquals("Swag Labs",driver.getTitle());
    }

}
