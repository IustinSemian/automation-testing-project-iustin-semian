package utils;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page_objects.Application;
import page_objects.LoginPage;

public class BaseClass {

    protected WebDriver driver;


    @Before
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        LoginPage login = new Application(driver).navigateToLoginPage();
        login.loginWith("standard_user" , "secret_sauce");
    }


    @After
    public void tearDown() {
        driver.quit();
    }

}
