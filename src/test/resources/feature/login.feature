@Login
Feature: Login tests

  @Positive
  Scenario: Login with valid user


    Given Be on the login page

    When Put the username
    And Put the password
    And CLick Login button

    Then Check the logo displayed

  @Negative
  Scenario: Login with locked out user

    Given Be on the login page

    When Put the locked out username
    And Put the password
    And CLick Login button

    Then Check the logo displayed


  @Negative
  Scenario: Login with problem user

    Given Be on the login page

    When Put the problem username
    And Put the password
    And CLick Login button

    Then Check the logo displayed

    @Negative

    Scenario: Login with performance glitch user

      Given Be on the login page

      When Put the glitch username
      And Put the password
      And CLick Login button

      Then Check the logo displayed