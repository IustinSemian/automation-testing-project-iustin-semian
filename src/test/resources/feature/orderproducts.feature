@Regression

Feature: Order products

  Scenario: Order a Sauce Labs Backpack

    Given Access the login page

    When Insert the username
    And Insert the password
    And Press the Login button

    When Add to cart Backpack
    And Press the shopping cart button
    And Press the checkout button
    And Fill the First name
    And Fill Last name
    And Fill Postal code

    When Press Continue button
    And Press the finish button
    And Press the back to products button
    And Open the hamburger menu
    And Press the logout button